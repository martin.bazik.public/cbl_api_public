# cbl_api

Baseline API service that meets overall system requirements and uses three input data 
sources:
1. Energy consumption time series data is provided in energy.csv in right-labelled format,
2. Past events data is provided in events.csv,
3. Holidays are sourced from 2023-list-of-holidays.pdf

## Development

### Create env and install requirents
```
python -m pip venv .venv
.\.venv\Script\activate
cd cbl_api
pip install -r requirements.txt
```


### Convert holidays to csv
```
python .\seed\pdf_to_csv.py ..\data\2023-list-of-holidays.pdf ..\data\holidays.csv
```

### Create fixtures from csv 
```
python .\seed\csv_to_json.py [events,energy,holidays]
```

### Load fixtures one by one
```
python .\manage.py loaddata api\fixtures\[events,energy,holidays].json 
```

### Run dev server
```
python .\manage.py runserver 
```

### Run tests
```
python .\manage.py test --keepdb
```


## Docker compose
There is a docker compose file with postgres that open port 8000 for the baseline API.
```
docker-compose up
```

## Example calls 
POST /baseline/      - data: 
```
{
    "building_id": "Building E",
    "timezone": "America/New_York",
    "start": "2024-01-07T12:00:00Z",
    "end": "2024-01-07T15:00:00Z",
    "granularity": "1hour"
}
```
Output:
```
{
    "2024-01-07T12:00:00Z": 205455.26560640335,
    "2024-01-07T13:00:00Z": 205595.72015857697,
    "2024-01-07T14:00:00Z": 211504.37919855118
}
```

## Known Issues:
fix possible issues in data, timezone not used, kWh-kW conversion