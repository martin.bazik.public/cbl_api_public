from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from datetime import datetime
import pytz

class BaselineCalculationViewTests(APITestCase):
    def test_baseline_calculation_weekday_success(self):
        url = reverse('baseline-calculation')
        data = {
            'building_id': "Building E",
            'timezone': 'America/New_York',
            'start': '2024-01-01T12:00:00Z',
            'end': '2024-01-01T15:00:00Z',
            'granularity': '1hour'
        }

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {'2024-01-01T12:00:00Z': 305801.45334968565,'2024-01-01T13:00:00Z': 343415.7599992752,'2024-01-01T14:00:00Z': 357669.3941829681})


    def test_baseline_calculation_weekday_with_events_success(self):
        url = reverse('baseline-calculation')
        data = {
            'building_id': "Building E",
            'timezone': 'America/New_York',
            'start': '2023-02-23T12:00:00Z',
            'end': '2023-02-23T15:00:00Z',
            'granularity': '1hour'
        }

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {'2023-02-23T12:00:00Z': 365181.3568231583, '2023-02-23T13:00:00Z': 389899.72864227294, '2023-02-23T14:00:00Z': 408307.4534656525})


    def test_baseline_calculation_weekend_success(self):
        url = reverse('baseline-calculation')
        data = {
            'building_id': "Building E",
            'timezone': 'America/New_York',
            'start': '2024-01-07T12:00:00Z',
            'end': '2024-01-07T15:00:00Z',
            'granularity': '1hour'
        }

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {'2024-01-07T12:00:00Z': 205455.26560640335, '2024-01-07T13:00:00Z': 205595.72015857697,'2024-01-07T14:00:00Z': 211504.37919855118})

    def test_baseline_calculation_weekend_15min_success(self):
        url = reverse('baseline-calculation')
        data = {
            'building_id': "Building E",
            'timezone': 'America/New_York',
            'start': '2024-01-07T12:00:00Z',
            'end': '2024-01-07T13:00:00Z',
            'granularity': '15min'
        }

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {'2024-01-07T12:00:00Z': 206816.4168548584,'2024-01-07T12:15:00Z': 206452.4515171051,'2024-01-07T12:30:00Z': 203400.17654037476,'2024-01-07T12:45:00Z': 205152.01751327515})


    def test_baseline_calculation_invalid_data(self):
        url = reverse('baseline-calculation')
        data = {
            'building_id': "Building E",
            'timezone': 'Invalid/Timezone',
            'start': '2024-01-01T00:00:00Z',
            'end': '2024-01-02T00:00:00Z',
            'granularity': 'invalid_granularity'
        }

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)