import sys
import datetime

from rest_framework.views import APIView
from rest_framework.response import Response

import logging
import pandas as pd

from api.serializers import BaselineInputSerializer
from api.models import Energy
from api.utils import *

logger = logging.getLogger("dev")
logger.setLevel(logging.INFO)
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(logging.INFO)
logger.addHandler(stream_handler)



# Create your views here.
class BaselineCalculationView(APIView):
    def post(self, request):
        serializer = BaselineInputSerializer(data=request.data)
        if serializer.is_valid():
            # Perform baseline calculation
            # Access validated data using serializer.validated_data
            building_id: str = serializer.validated_data['building_id']
            timezone: str = serializer.validated_data['timezone'] # TODO: missing implementation
            start: datetime.datetime = serializer.validated_data['start']
            end: datetime.datetime = serializer.validated_data['end']
            granularity: str = serializer.validated_data['granularity']
            
            match granularity:
                case "5min":
                    hours = 1/12
                    freq = granularity
                case "15min":
                    hours = 0.25 
                    freq = granularity
                case "1hour":
                    hours = 1
                    freq = "1h"
                
            
            event_start_window_day: int = start.weekday()

            is_weekday: bool = event_start_window_day < 5
            
            energy_qs = Energy.objects.filter(building_id=building_id)
            
            if is_weekday:
                filtered_past_30_days_intervals = get_weekdays_last_30_days(start,end)
                
                energy_window_qs = filter_window_energy_entries(energy_qs, filtered_past_30_days_intervals)
                grouped_days_series, grouped_df = group_series_days(energy_window_qs, freq)
                grouped_days_series = get_ten_weekday_series(grouped_days_series)
                average_cbl_per_interval = get_n_top_entries(grouped_days_series,grouped_df,5)
            else:
                last_three_weekend_day_windows = [(start - datetime.timedelta(weeks=i+1),end - datetime.timedelta(weeks=i+1)) for i in range(3)]
                energy_window_qs = filter_window_energy_entries(energy_qs, last_three_weekend_day_windows)        
                
                grouped_days_series, grouped_df = group_series_days(energy_window_qs, freq)
                average_cbl_per_interval = get_n_top_entries(grouped_days_series,grouped_df,2)
            average_cbl_per_interval.index = pd.to_datetime(average_cbl_per_interval.index.map(lambda x: start.replace(hour=x.hour, minute=x.minute, second=x.second))).strftime('%Y-%m-%dT%H:%M:%SZ')
            res_time_series = average_cbl_per_interval.to_dict()
            res_time_series: dict = res_time_series["cumulative_energy_kwh"]
            # convert to kW
            res_time_series = {k: v/hours for k, v in  res_time_series.items()}
            
            

            # Perform baseline calculation logic here
            
            return Response(res_time_series)
        else:
            return Response(serializer.errors, status=400)