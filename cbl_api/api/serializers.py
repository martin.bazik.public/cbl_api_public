from rest_framework import serializers
import pytz
from api.models import Energy

class BaselineInputSerializer(serializers.Serializer):
    building_id = serializers.CharField()
    timezone = serializers.CharField()
    start = serializers.DateTimeField()
    end = serializers.DateTimeField()
    granularity = serializers.ChoiceField(choices=['5min', '15min', '1hour'])
    
    def validate_timezone(self, value):
        # Check if the timezone is valid
        if value not in pytz.all_timezones:
            raise serializers.ValidationError("Invalid timezone")

        return value
    
    def validate_building_id(self, value):
        # Check if the timezone is valid
        if not Energy.objects.filter(building_id=value).exists():
            raise serializers.ValidationError("Building with that id not present")

        return value