from django.db import models

# Create your models here.
class Utility:
  CON_ED = "ConEd"
  
class ISO:
  NYISO = "NYISO"


class Events(models.Model):
  identifier = models.UUIDField(primary_key=True)
  ISO = models.CharField(max_length=64)
  utility_id = models.CharField(max_length=64)
  program_name = models.CharField(max_length=64)
  event_start = models.DateTimeField()
  event_end = models.DateTimeField()
  timezone = models.CharField(max_length=64)

class Energy(models.Model):
  identifier = models.UUIDField(primary_key=True)
  building_id = models.CharField(max_length=64)
  timezone = models.CharField(max_length=64)
  timestamp = models.DateTimeField()
  cumulative_energy_kwh = models.FloatField()

class Holidays(models.Model):
  identifier = models.UUIDField(primary_key=True)
  date = models.DateField()
  holiday = models.CharField(max_length=64)
  