import datetime
from django.db.models import Q
import pandas as pd

from api.models import Events, Holidays, Utility, ISO


def get_weekdays_last_30_days(start,end):
    past_30_days: list = []
    past_30_days_intervals: list = []
    
    for i in range(30):
        delta = datetime.timedelta(days=i+1)
        previous_day_start = start - delta
        previous_day_end = end - delta
        
        past_30_days.append(previous_day_start.date())
        past_30_days_intervals.append((previous_day_start,previous_day_end))
    
    excluded_dates: set = set({})
    
    events_in_window_qs = Events.objects.filter(event_start__date__in=past_30_days)
    
    con_ed_condition = Q(utility_id=Utility.CON_ED, program_name__in=["DLRP", "CSRP", "Term-DLM", "Auto-DLM"])
    nyiso_condition = Q(ISO=ISO.NYISO, program_name__in=[ "SCR", "EDRP", "TDRP"])
    filtered_events = events_in_window_qs.filter(Q(con_ed_condition) | Q(nyiso_condition))
    
    one_day_delta = datetime.timedelta(days=1)
    for event in filtered_events:
        excluded_dates.add(event.event_start.date())
        excluded_dates.add(event.event_start.date()-one_day_delta)
        
    holidays = Holidays.objects.filter(date__in=past_30_days)
    for holiday in holidays:
        excluded_dates.add(holiday.date)
        
    filtered_past_30_days_intervals = [interval for interval in past_30_days_intervals if interval[0].date() not in excluded_dates and interval[0].weekday() < 5]
    return filtered_past_30_days_intervals

def min_max_difference(group):
    # print(group.max())
    # print(group.min())
    
    return group.max() - group.min()



def calc_average(selected_energy_values):
    return sum(selected_energy_values)/len(selected_energy_values)

def filter_window_energy_entries(energy_qs, list_of_intervals: list):
    q_objects = Q()
    for start, end in list_of_intervals:
        q_objects |= Q(timestamp__range=(start, end))
    energy_window = energy_qs.filter(q_objects)
    return energy_window

def group_series_days(energy_window_qs, freq):
    df = pd.DataFrame(list(energy_window_qs.values()))
                
    # create intervals base on granularity
    groups = df.groupby(
        pd.Grouper(key="timestamp", freq=freq, origin="start_day", label="left")
    )
    
    diff_between_minimums = groups['cumulative_energy_kwh'].min().diff().shift(-1)
    cleaned_diff_between_minimums = diff_between_minimums.dropna()
    grouped_df = cleaned_diff_between_minimums.to_frame()
    
    grouped_days_series = grouped_df.groupby(pd.Grouper(freq='D'))['cumulative_energy_kwh'].mean().dropna()
    return grouped_days_series, grouped_df


def get_ten_weekday_series(grouped_days_series):
    grouped_days_df = grouped_days_series.sort_index(ascending=False).to_frame()
    selected_energy_dates = []
    selected_energy_values = []                

    for index, day_energy in grouped_days_df.iterrows():
        if len(selected_energy_values) >= 10:
            continue
        
        if len(selected_energy_values) == 0 or day_energy["cumulative_energy_kwh"] >=  0.25 * calc_average(selected_energy_values):
            selected_energy_dates.append(index.date())
            selected_energy_values.append(day_energy["cumulative_energy_kwh"])
    
    index_dates_series = pd.Series(grouped_days_series.index.date)
    boolean_index = index_dates_series.isin(selected_energy_dates).tolist()
    grouped_days_series = grouped_days_series[boolean_index]
    return grouped_days_series

def get_n_top_entries(grouped_days_series: pd.Series, grouped_df: pd.DataFrame,n=1) -> pd.Series:
    top_two_grouped_days_series = grouped_days_series.sort_values(ascending=False)[:n]
    index_dates = top_two_grouped_days_series.index.date.tolist()
    index_dates_series = pd.Series(grouped_df.index.date)
    boolean_index = index_dates_series.isin(index_dates).tolist()
    filtered_df = grouped_df[boolean_index]
    average_cbl_per_interval = filtered_df.groupby(filtered_df.index.time).mean()
    return average_cbl_per_interval
