python ./manage.py migrate &&
python ./manage.py loaddata api/fixtures/events.json &&
python ./manage.py loaddata api/fixtures/energy.json &&
python ./manage.py loaddata api/fixtures/holidays.json &&
gunicorn --bind 0.0.0.0:8000 cbl_api.wsgi:application