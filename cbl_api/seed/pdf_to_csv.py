import pdfplumber
import sys
import json
import pandas as pd

def extract_tables_from_pdf(pdf_path):
    tables = []
    with pdfplumber.open(pdf_path) as pdf:
        for page in pdf.pages:
            tables.extend(page.extract_tables())
    return tables

def create_fixture(data, output_file):
    # Write JSON data to fixture file
    with open(output_file, 'w') as f:
        json.dump(data, f)

if __name__ == "__main__":
    pdf_path = sys.argv[1]
    tables = extract_tables_from_pdf(pdf_path)
    if len(tables) == 1:
        table = tables[0]
        df = pd.DataFrame(table[1:], columns=table[0])
        df['Date'] = df['Date'].str.rstrip('*')
        df['Date'] = df['Date'].apply(lambda x: f"{x}, 2023" if len(x.split(" ")) == 2 else x)
        df['Date'] = pd.to_datetime(df['Date'], format='%B %d, %Y')
        df.columns = df.columns.str.lower()
        df.to_csv(sys.argv[2], index=False)