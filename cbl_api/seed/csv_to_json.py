import csv
import json
import sys
import os

    
def csv_to_json(csv_file,model):
    # Read CSV file and convert to JSON
    data = []
    with open(csv_file, 'r') as f:
        reader = csv.DictReader(f)
        for i,row in enumerate(reader):
            data.append({
                "model": model,
                "pk": i,
                "fields": row
            })
    return data

def create_fixture(data, output_file):
    # Write JSON data to fixture file
    with open(output_file, 'w') as f:
        json.dump(data, f)

if __name__ == "__main__":
    data_srcs = ["energy","events","holidays"]
    if sys.argv[1] in data_srcs:
        csv_file = os.path.join('..','data',f'{sys.argv[1]}.csv')
        output_file = os.path.join('api','fixtures',f'{sys.argv[1]}.json')
        model = f"api.{sys.argv[1]}"
    else:
        print(f"invalid input, not in {data_srcs}")
        

    
    data = csv_to_json(csv_file,model)
    create_fixture(data, output_file)